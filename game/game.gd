extends Spatial

onready var white_ball: RigidBody = $Balls/WhiteBall
onready var table: Spatial = $Table
onready var camera: Camera = $Camera
onready var movement_prediction_drawer: ImmediateGeometry = $MovementPredictionDrawer


class WhiteBallCurrentMouseState:
	var clicked := false
	var dragged := false
	var drag_offset := Vector3.ZERO


var _white_ball_current_mouse_state := WhiteBallCurrentMouseState.new()


func _physics_process(delta: float):
	if _white_ball_current_mouse_state.dragged:
		if not _white_ball_current_mouse_state.clicked:
			# Ball was released
			_white_ball_current_mouse_state.dragged = false
			white_ball.apply_central_impulse(_white_ball_current_mouse_state.drag_offset * -1.0)

		var result := _predict_movement()
		if result.collider != null:
			movement_prediction_drawer.clear()
			movement_prediction_drawer.begin(Mesh.PRIMITIVE_LINES)
			movement_prediction_drawer.add_vertex(white_ball.global_transform.origin)
			movement_prediction_drawer.add_vertex(result.collision_point)
			movement_prediction_drawer.end()


func _input(event: InputEvent) -> void:
	if not _white_ball_current_mouse_state.clicked:
		return

	if event is InputEventMouseMotion:
		var offset = (
			camera.project_position(
				event.position,
				camera.global_transform.origin.y - white_ball.global_transform.origin.y
			)
			- white_ball.global_transform.origin
		)
		_white_ball_current_mouse_state.drag_offset = offset

		if _white_ball_current_mouse_state.dragged and offset.length() <= 0.25:
			_white_ball_current_mouse_state.dragged = false
		elif not _white_ball_current_mouse_state.dragged and offset.length() > 0.25:
			_white_ball_current_mouse_state.dragged = true
	elif event is InputEventMouseButton and not event.is_pressed():
		_white_ball_current_mouse_state.clicked = false


func _on_white_ball_input_event(camera, event: InputEvent, position, normal, shape_idx) -> void:
	if event is InputEventMouseButton:
		_white_ball_current_mouse_state.clicked = event.is_pressed()


func _predict_movement() -> PhysicsTestMotionResult:
	var result := PhysicsTestMotionResult.new()
	var steps := 0
	var from := white_ball.global_transform
	var motion := _white_ball_current_mouse_state.drag_offset * -1.0
	while result.collider == null and steps < 5:
		PhysicsServer.body_test_motion(
			white_ball.get_rid(), from, motion, false, result, true, [table.get_node("Base")]
		)
		from.origin += motion
		steps += 1
	return result
